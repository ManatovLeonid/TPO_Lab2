package Abstracts;

public abstract class CalculationFunction {
    public abstract double Calculate(double x,double percision);
}
