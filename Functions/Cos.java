package Functions;

import Abstracts.AbstractCos;

import java.util.stream.LongStream;

public class Cos extends AbstractCos {

    public double Calculate(double x, double percision) {

        double accum = 1;
        double fact = 1;
        int counter = 2;
        double member = accum;
        while (Math.abs(member) >= percision) {
            fact *= ((counter) * (counter - 1));
            member = Math.pow(x, counter) / fact;
            if (counter % 4 == 2) {
                member *= -1;
            }
            accum += member;
            counter += 2;
        }
        return accum;
    }


}
