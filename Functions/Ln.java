package Functions;

import Abstracts.AbstractLn;

public class Ln extends AbstractLn {


    @Override
    public double Calculate(double x, double percision) {

        if (x <=0) return Double.NaN;


        double arg = (x - 1) / (x + 1);
        double res = arg;
        double member = arg;
        double prev_res;
        int counter = 3;
        do {
            prev_res = member;
            member = Math.pow(arg, counter) / (double) counter;
            res += member;
            counter += 2;
        } while (2 * Math.abs(prev_res - member) >= percision/100);
        return 2 * res;


    }
}
