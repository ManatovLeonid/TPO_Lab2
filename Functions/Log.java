package Functions;

import Abstracts.AbstractLn;
import Abstracts.AbstractLog;

public class Log extends AbstractLog {

    private AbstractLn ln;
    private int base;

    public Log(AbstractLn ln, int base) {
        this.ln = ln;
        this.base = base;
    }

    @Override
    public double Calculate(double x, double percision) {
        return ln.Calculate(x, percision) / ln.Calculate(base, percision);
    }
}
