package Functions;

import Abstracts.AbstractCos;
import Abstracts.AbstractSec;

public class Sec extends AbstractSec {

    private AbstractCos cos;

    public Sec(AbstractCos cos) {
        this.cos = cos;
    }

    @Override
    public double Calculate(double x, double percision) {

        if (Math.abs(x % Math.PI) == Math.PI / 2)
            return Double.NaN;

        return 1 / cos.Calculate(x%(2* Math.PI), percision);
    }
}
