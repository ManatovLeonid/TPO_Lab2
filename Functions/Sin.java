package Functions;

import Abstracts.AbstractCos;
import Abstracts.AbstractSin;

public class Sin extends AbstractSin {

    private AbstractCos cos;

    public Sin(AbstractCos cos) {
        this.cos = cos;
    }

    @Override
    public double Calculate(double x, double percision) {

        return cos.Calculate(Math.PI / 2 - x, percision);
    }
}
