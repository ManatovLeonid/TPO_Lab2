package Functions;

import Abstracts.AbstractCos;
import Abstracts.AbstractSin;
import Abstracts.AbstractTan;

public class Tan extends AbstractTan {

    private AbstractCos cos;
    private AbstractSin sin;

    public Tan(AbstractCos cos, AbstractSin sin) {
        this.cos = cos;
        this.sin = sin;
    }

    @Override
    public double Calculate(double x, double percision) {

        if (Math.abs(x % Math.PI) == Math.PI / 2)
            return Double.NaN;

        return sin.Calculate(x, percision) / cos.Calculate(x, percision);
    }
}
