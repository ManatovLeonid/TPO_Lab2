package TaskTesting;

import Abstracts.*;

public class TaskSystem {
    protected AbstractCos cos;
    protected AbstractSin sin;
    protected AbstractTan tan;
    protected AbstractSec sec;

    protected AbstractLn ln;
    protected AbstractLog log2;
    protected AbstractLog log3;
    protected AbstractLog log5;
    protected AbstractLog log10;


    public TaskSystem(AbstractCos cos, AbstractSin sin, AbstractTan tan, AbstractSec sec, AbstractLn ln, AbstractLog log2, AbstractLog log3, AbstractLog log5, AbstractLog log10) {
        this.cos = cos;
        this.sin = sin;
        this.tan = tan;
        this.sec = sec;
        this.ln = ln;
        this.log2 = log2;
        this.log3 = log3;
        this.log5 = log5;
        this.log10 = log10;
    }

    private double BeforeZeroPart(double x) {

        if (Math.abs( Math.abs(x % Math.PI)- Math.PI / 2)<0.01) return Double.NaN;

        double perc = 0.01;
        double tanVal = tan.Calculate(x, perc);
        return tanVal * tanVal * tanVal / (sec.Calculate(x, perc) - sin.Calculate(x, perc));
    }

    private double AfterZeroPart(double x) {
        double perc = 0.01;

        double log10Val = log10.Calculate(x, perc);
        double log5Val = log5.Calculate(x, perc);
        double log3Val = log3.Calculate(x, perc);
        double log2Val = log2.Calculate(x, perc);
        double lnVal = ln.Calculate(x, perc);

        return (((((log10Val + log2Val) + log5Val) + log3Val) - log3Val) * (lnVal + ((log10Val - log3Val) * (log5Val * log5Val))));
    }

    public double GetValue(double x) {

        if (x > 0) return AfterZeroPart(x);
        else return BeforeZeroPart(x);
    }

}
