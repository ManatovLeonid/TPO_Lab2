package TaskTesting;

import Abstracts.*;
import Functions.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;

import java.util.function.Function;

public class TaskSystemTests {


    public static <T extends CalculationFunction> T GetFunctionMock(Class<T> tClass, Function<Double, Double> func) {
        T mock = Mockito.mock(tClass);
        ArgumentCaptor<Double> x = ArgumentCaptor.forClass(Double.class);

        Mockito.when(mock.Calculate(x.capture(), Mockito.anyDouble())).thenAnswer(
                (Answer) invocation -> func.apply(x.getValue()));

        return mock;
    }

    public static AbstractSin GetSinMock() {
        return GetFunctionMock(AbstractSin.class, Math::sin);
    }

    public static AbstractCos GetCosMock() {
        return GetFunctionMock(AbstractCos.class, Math::cos);
    }

    public static AbstractSec GetSecMock() {
        return GetFunctionMock(AbstractSec.class, a -> 1 / Math.cos(a));
    }

    public static AbstractTan GetTanMock() {
        return GetFunctionMock(AbstractTan.class, Math::tan);
    }

    public static AbstractLn GetLnMock() {
        return GetFunctionMock(AbstractLn.class, Math::log);
    }

    public static AbstractLog GetLogMock(int base) {
        return GetFunctionMock(AbstractLog.class, a -> Math.log(a) / Math.log(base));
    }


    /*
    Тестировние Снизу вверх

    Cos -> Sec -> Sin -> Tan
    Ln -> Log

     */

    protected TaskSystem taskSystem;

    @BeforeEach
    public void TaskMocksSetUp() {

        AbstractCos cos = GetCosMock();
        AbstractSin sin = GetSinMock();
        AbstractTan tan = GetTanMock();
        AbstractSec sec = GetSecMock();
        AbstractLn ln = GetLnMock();
        AbstractLog log2 = GetLogMock(2);
        AbstractLog log3 = GetLogMock(3);
        AbstractLog log5 = GetLogMock(5);
        AbstractLog log10 = GetLogMock(10);

        taskSystem = new TaskSystem(cos, sin, tan, sec, ln, log2, log3, log5, log10);
    }

    @ParameterizedTest
    @ValueSource(doubles = {0, Math.PI * -1, Math.PI * -2, Math.PI * -5})
    public void LeftRootsTest(double testX) {
        double perc = 0.01;
        Assertions.assertEquals(0, taskSystem.GetValue(testX), perc);
    }

    @ParameterizedTest
    @ValueSource(doubles = {0.097, 1, 10.308})
    public void RightRootsTest(double testX) {
        double perc = 0.1;
        Assertions.assertEquals(0, taskSystem.GetValue(testX), perc);
    }

    @ParameterizedTest
    @ValueSource(doubles = {-1.5 * Math.PI, -2.5 * Math.PI, -3.5 * Math.PI})
    public void LeftNanRootsTest(double testX) {
        double perc = 0.01;
        Assertions.assertEquals(Double.NaN, taskSystem.GetValue(testX), perc);
    }

    @Test
    public void RightPointsRootsTest() {
        double perc = 0.01;
        Assertions.assertEquals(3.399, taskSystem.GetValue(0.192), perc);
        Assertions.assertEquals(3.399, taskSystem.GetValue(5.205), perc);
    }


}


class TaskCos extends TaskSystemTests {

    @BeforeEach
    @Override
    public void TaskMocksSetUp() {

        AbstractCos cos = new Cos();
        AbstractSin sin = GetSinMock();
        AbstractTan tan = GetTanMock();
        AbstractSec sec = GetSecMock();
        AbstractLn ln = GetLnMock();
        AbstractLog log2 = GetLogMock(2);
        AbstractLog log3 = GetLogMock(3);
        AbstractLog log5 = GetLogMock(5);
        AbstractLog log10 = GetLogMock(10);

        taskSystem = new TaskSystem(cos, sin, tan, sec, ln, log2, log3, log5, log10);
    }

}


class TaskCosSin extends TaskSystemTests {

    @BeforeEach
    @Override
    public void TaskMocksSetUp() {

        AbstractCos cos = new Cos();
        AbstractSin sin = new Sin(cos);
        AbstractTan tan = GetTanMock();
        AbstractSec sec = GetSecMock();
        AbstractLn ln = GetLnMock();
        AbstractLog log2 = GetLogMock(2);
        AbstractLog log3 = GetLogMock(3);
        AbstractLog log5 = GetLogMock(5);
        AbstractLog log10 = GetLogMock(10);

        taskSystem = new TaskSystem(cos, sin, tan, sec, ln, log2, log3, log5, log10);
    }

}


class TaskCosSinTan extends TaskSystemTests {

    @BeforeEach
    @Override
    public void TaskMocksSetUp() {

        AbstractCos cos = new Cos();
        AbstractSin sin = new Sin(cos);
        AbstractTan tan = new Tan(cos, sin);
        AbstractSec sec = GetSecMock();
        AbstractLn ln = GetLnMock();
        AbstractLog log2 = GetLogMock(2);
        AbstractLog log3 = GetLogMock(3);
        AbstractLog log5 = GetLogMock(5);
        AbstractLog log10 = GetLogMock(10);

        taskSystem = new TaskSystem(cos, sin, tan, sec, ln, log2, log3, log5, log10);
    }

}


class TaskCosSinTanSec extends TaskSystemTests {

    @BeforeEach
    @Override
    public void TaskMocksSetUp() {

        AbstractCos cos = new Cos();
        AbstractSin sin = new Sin(cos);
        AbstractTan tan = new Tan(cos, sin);
        AbstractSec sec = new Sec(cos);
        AbstractLn ln = GetLnMock();
        AbstractLog log2 = GetLogMock(2);
        AbstractLog log3 = GetLogMock(3);
        AbstractLog log5 = GetLogMock(5);
        AbstractLog log10 = GetLogMock(10);

        taskSystem = new TaskSystem(cos, sin, tan, sec, ln, log2, log3, log5, log10);
    }

}

class TaskCosSinTanSecLn extends TaskSystemTests {

    @BeforeEach
    @Override
    public void TaskMocksSetUp() {

        AbstractCos cos = new Cos();
        AbstractSin sin = new Sin(cos);
        AbstractTan tan = new Tan(cos, sin);
        AbstractSec sec = new Sec(cos);
        AbstractLn ln = new Ln();
        AbstractLog log2 = GetLogMock(2);
        AbstractLog log3 = GetLogMock(3);
        AbstractLog log5 = GetLogMock(5);
        AbstractLog log10 = GetLogMock(10);

        taskSystem = new TaskSystem(cos, sin, tan, sec, ln, log2, log3, log5, log10);
    }

}

class TaskCosSinTanSecLnLog2 extends TaskSystemTests {

    @BeforeEach
    @Override
    public void TaskMocksSetUp() {

        AbstractCos cos = new Cos();
        AbstractSin sin = new Sin(cos);
        AbstractTan tan = new Tan(cos, sin);
        AbstractSec sec = new Sec(cos);
        AbstractLn ln = new Ln();
        AbstractLog log2 = new Log(ln, 2);
        AbstractLog log3 = GetLogMock(3);
        AbstractLog log5 = GetLogMock(5);
        AbstractLog log10 = GetLogMock(10);

        taskSystem = new TaskSystem(cos, sin, tan, sec, ln, log2, log3, log5, log10);
    }

}

class TaskCosSinTanSecLnLog2Log3 extends TaskSystemTests {

    @BeforeEach
    @Override
    public void TaskMocksSetUp() {

        AbstractCos cos = new Cos();
        AbstractSin sin = new Sin(cos);
        AbstractTan tan = new Tan(cos, sin);
        AbstractSec sec = new Sec(cos);
        AbstractLn ln = new Ln();
        AbstractLog log2 = new Log(ln, 2);
        AbstractLog log3 = new Log(ln, 3);
        AbstractLog log5 = GetLogMock(5);
        AbstractLog log10 = GetLogMock(10);

        taskSystem = new TaskSystem(cos, sin, tan, sec, ln, log2, log3, log5, log10);
    }

}

class TaskCosSinTanSecLnLog2Log3Log5 extends TaskSystemTests {

    @BeforeEach
    @Override
    public void TaskMocksSetUp() {

        AbstractCos cos = new Cos();
        AbstractSin sin = new Sin(cos);
        AbstractTan tan = new Tan(cos, sin);
        AbstractSec sec = new Sec(cos);
        AbstractLn ln = new Ln();
        AbstractLog log2 = new Log(ln, 2);
        AbstractLog log3 = new Log(ln, 3);
        AbstractLog log5 = new Log(ln, 5);
        AbstractLog log10 = GetLogMock(10);

        taskSystem = new TaskSystem(cos, sin, tan, sec, ln, log2, log3, log5, log10);
    }

}

class TaskCosSinTanSecLnLog2Log3Log5Log10 extends TaskSystemTests {

    @BeforeEach
    @Override
    public void TaskMocksSetUp() {

        AbstractCos cos = new Cos();
        AbstractSin sin = new Sin(cos);
        AbstractTan tan = new Tan(cos, sin);
        AbstractSec sec = new Sec(cos);
        AbstractLn ln = new Ln();
        AbstractLog log2 = new Log(ln, 2);
        AbstractLog log3 = new Log(ln, 3);
        AbstractLog log5 = new Log(ln, 5);
        AbstractLog log10 = new Log(ln, 10);

        taskSystem = new TaskSystem(cos, sin, tan, sec, ln, log2, log3, log5, log10);
    }

}

