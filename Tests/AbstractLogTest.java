package Tests;

import Abstracts.AbstractCos;
import Abstracts.AbstractLn;
import Functions.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.List;


abstract class AbstractLogTest {


    protected AbstractLn log;

    @BeforeEach
    public abstract void SetUpLog();

    public static List<Double> Before1Generator() {


        List<Double> arguments = new ArrayList<Double>();

        for (double x = 0.1; x < 1; x += 0.1) {
            arguments.add(x);
        }

        return arguments;
    }

    public static List<Double> After1Generator() {


        List<Double> arguments = new ArrayList<Double>();

        for (double x = 1.1; x < 10; x += 2) {
            arguments.add(x);
        }

        return arguments;
    }

    public static List<Double> NanBefore0Generator() {


        List<Double> arguments = new ArrayList<Double>();

        for (double x = -10; x < 0; x += 2) {
            arguments.add(x);
        }

        return arguments;
    }

    @Test
    protected void OneTest() {
        double perc = 0.01;
        Assertions.assertEquals(0, log.Calculate(1, perc), perc);
    }

    @Test
    protected void ZeroTest() {
        double perc = 0.01;
        Assertions.assertEquals(Double.NaN, log.Calculate(0, perc), perc);
    }

    @ParameterizedTest
    @MethodSource("Before1Generator")
    void Before1Test(double textX) {
        SampleLnTest(textX);
    }


    @ParameterizedTest
    @MethodSource("After1Generator")
    void After1Test(double textX) {
        SampleLnTest(textX);
    }

    @ParameterizedTest
    @MethodSource("NanBefore0Generator")
    void NanBefore0Test(double textX) {
        NanLnTest(textX);
    }


    protected abstract void SampleLnTest(double testX);

    protected abstract void NanLnTest(double testX);
}



class LnTest extends AbstractLogTest {
    @BeforeEach
    @Override
    public void SetUpLog() {
        log = new Ln();
    }

    @Override
    protected void SampleLnTest(double testX) {
        double perc = 0.001;
        Assertions.assertEquals(Math.log(testX), log.Calculate(testX, perc), 0.01);
    }

    @Override
    protected void NanLnTest(double testX) {
        double perc = 0.001;
        Assertions.assertEquals(Double.NaN, log.Calculate(testX, perc), 0.01);
    }
}



class IntegrationLogsTest extends LogsTest {

    @BeforeEach
    @Override
    public void SetUpLog() {
        AbstractLn mockLn = Mockito.mock(AbstractLn.class);
        ArgumentCaptor<Double> x = ArgumentCaptor.forClass(Double.class);

        Mockito.when(mockLn.Calculate(x.capture(), Mockito.anyDouble())).thenAnswer(
                (Answer) invocation -> Math.log(x.getValue()));


        log2 = new Log(mockLn, 2);
        log3 = new Log(mockLn, 3);
        log5 = new Log(mockLn, 5);
        log10 = new Log(mockLn, 10);
    }


}

class LogsTest extends AbstractLogTest {

    protected Log log2;
    protected Log log3;
    protected Log log5;
    protected Log log10;

    @BeforeEach
    @Override
    public void SetUpLog() {

        log2 = new Log(new Ln(), 2);
        log3 = new Log(new Ln(), 3);
        log5 = new Log(new Ln(), 5);
        log10 = new Log(new Ln(), 10);
    }

    @Override
    protected void SampleLnTest(double testX) {
        double perc = 0.001;
        Assertions.assertEquals(Math.log(testX) / Math.log(2), log2.Calculate(testX, perc), 0.01);
        Assertions.assertEquals(Math.log(testX) / Math.log(3), log3.Calculate(testX, perc), 0.01);
        Assertions.assertEquals(Math.log(testX) / Math.log(5), log5.Calculate(testX, perc), 0.01);
        Assertions.assertEquals(Math.log(testX) / Math.log(10), log10.Calculate(testX, perc), 0.01);

    }

    @Override
    protected void NanLnTest(double testX) {
        double perc = 0.001;
        Assertions.assertEquals(Double.NaN, log2.Calculate(testX, perc), 0.01);
        Assertions.assertEquals(Double.NaN, log3.Calculate(testX, perc), 0.01);
        Assertions.assertEquals(Double.NaN, log5.Calculate(testX, perc), 0.01);
        Assertions.assertEquals(Double.NaN, log10.Calculate(testX, perc), 0.01);

    }

    @Override
    protected void OneTest() {
        double perc = 0.01;
        Assertions.assertEquals(0, log2.Calculate(1, perc), perc);
        Assertions.assertEquals(0, log3.Calculate(1, perc), perc);
        Assertions.assertEquals(0, log5.Calculate(1, perc), perc);
        Assertions.assertEquals(0, log10.Calculate(1, perc), perc);
    }

    @Override
    protected void ZeroTest() {

        double perc = 0.01;
        Assertions.assertEquals(Double.NaN, log2.Calculate(0, perc), perc);
        Assertions.assertEquals(Double.NaN, log3.Calculate(0, perc), perc);
        Assertions.assertEquals(Double.NaN, log5.Calculate(0, perc), perc);
        Assertions.assertEquals(Double.NaN, log10.Calculate(0, perc), perc);
    }
}
