package Tests;

import TaskTesting.TaskSystemTests;
import Functions.Cos;
import Functions.Sec;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.List;

class IntegrationSecTest extends SecTest {

    @BeforeEach
    @Override
    public void SetUpSec() {
        sec = new Sec(TaskSystemTests.GetCosMock());
    }

}

class SecTest {


    protected Sec sec;

    @BeforeEach
    public void SetUpSec() {

        Cos cos = new Cos();
        sec = new Sec(cos);
    }

    public static List<Double> AsyptsGenerator() {

        List<Double> arguments = new ArrayList<Double>();
        for (int n = -5; n < 5; n++) {
            arguments.add(Math.PI / 2 + Math.PI * n);
        }

        return arguments;
    }


    public static List<Double> RaisingUpGenerator() {
        List<Double> arguments = new ArrayList<Double>();
        for (double x = 0.1; x < Math.PI / 2; x += 0.3) {
            arguments.add(x);
        }
        return arguments;
    }

    public static List<Double> RaisingDownGenerator() {
        List<Double> arguments = new ArrayList<Double>();
        for (double x = Math.PI * -3 / 2 + 0.2; x < -Math.PI; x += 0.3) {
            arguments.add(x);
        }
        return arguments;
    }

    public static List<Double> DecreaseUpGenerator() {
        List<Double> arguments = new ArrayList<Double>();
        for (double x = -Math.PI / 2 + 0.1; x < 0; x += 0.3) {
            arguments.add(x);
        }
        return arguments;
    }


    public static List<Double> DecreaseDownGenerator() {
        List<Double> arguments = new ArrayList<Double>();
        for (double x = -Math.PI + 0.3; x < 0; x += 0.3) {
            arguments.add(x);
        }
        return arguments;
    }


    @ParameterizedTest
    @MethodSource("RaisingUpGenerator")
    void RaisingUpTest(double textX) {
        SampleSecTest(textX);
    }


    @ParameterizedTest
    @MethodSource("RaisingDownGenerator")
    void RaisingDownTest(double textX) {
        SampleSecTest(textX);
    }

    @ParameterizedTest
    @MethodSource("DecreaseDownGenerator")
    void DecreaseDownTest(double textX) {
        SampleSecTest(textX);
    }

    @ParameterizedTest
    @MethodSource("DecreaseUpGenerator")
    void DecreaseUpTest(double textX) {
        SampleSecTest(textX);
    }

    @ParameterizedTest
    @MethodSource("AsyptsGenerator")
    void AsyptsTest(double textX) {
        NanTanTest(textX);
    }

    void SampleSecTest(double testX) {
        double perc = 0.01;
        Assertions.assertEquals(1 / Math.cos(testX), sec.Calculate(testX, perc), perc);
    }

    void NanTanTest(double testX) {
        double perc = 0.01;
        Assertions.assertEquals(Double.NaN, sec.Calculate(testX, perc), perc);
    }
}