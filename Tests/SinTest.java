package Tests;

import Abstracts.AbstractCos;
import Functions.Cos;
import Functions.Sin;
import TaskTesting.TaskSystemTests;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

class IntegrationSinTest extends SinTest {

    @BeforeEach
    @Override
    public void SetUpSin() {
        sin = new Sin(TaskSystemTests.GetCosMock());
    }

}

class SinTest {


    protected Sin sin;

    @BeforeEach
    public void SetUpSin() {

        Cos cos = new Cos();

        sin = new Sin(cos);
    }

    public static List<Double> RootsGenerator() {

        List<Double> arguments = new ArrayList<Double>();
        for (int n = -5; n < 5; n++) {
            arguments.add(Math.PI * n);

        }

        return arguments;
    }


    public static List<Double> RaisingGenerator() {


        List<Double> arguments = new ArrayList<Double>();

        for (int n = -5; n < 5; n++) {
            arguments.add(-Math.PI / 4 + Math.PI * 2 * n);
            arguments.add(Math.PI / 4 + Math.PI * 2 * n);

        }

        return arguments;
    }


    public static List<Double> DecreaseGenerator() {


        List<Double> arguments = new ArrayList<Double>();

        for (int n = -5; n < 5; n++) {
            arguments.add(Math.PI / 2 + Math.PI / 4 + Math.PI * 2 * n);
            arguments.add(Math.PI + Math.PI / 4 + Math.PI * 2 * n);

        }

        return arguments;
    }


    @ParameterizedTest
    @MethodSource("RaisingGenerator")
    void RaisingTest(double textX) {
        SampleSinTest(textX);
    }

    @ParameterizedTest
    @MethodSource("DecreaseGenerator")
    void DecreaseTest(double textX) {
        SampleSinTest(textX);
    }

    @ParameterizedTest
    @MethodSource("RootsGenerator")
    void RootsTest(double textX) {
        SampleSinTest(textX);
    }

    void SampleSinTest(double testX) {
        double perc = 0.01;
        Assertions.assertEquals(Math.sin(testX), sin.Calculate(testX, perc), perc);
    }
}