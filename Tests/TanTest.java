package Tests;

import Functions.Cos;
import Functions.Sin;
import Functions.Tan;
import TaskTesting.TaskSystemTests;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import java.util.ArrayList;
import java.util.List;

class IntegrationTanTest extends TanTest {

    @BeforeEach
    @Override
    public void SetUpTan() {
        tan = new Tan(TaskSystemTests.GetCosMock(), TaskSystemTests.GetSinMock());
    }

}

class TanTest {


    protected Tan tan;

    @BeforeEach
    public void SetUpTan() {

        Cos cos = new Cos();
        Sin sin = new Sin(cos);
        tan = new Tan(cos, sin);
    }

    public static List<Double> RootsGenerator() {

        List<Double> arguments = new ArrayList<Double>();
        for (int n = -5; n < 5; n++) {
            arguments.add(Math.PI * n);
        }

        return arguments;
    }

    public static List<Double> AsyptsGenerator() {

        List<Double> arguments = new ArrayList<Double>();
        for (int n = -5; n < 5; n++) {
            arguments.add(Math.PI / 2 + Math.PI * n);
        }

        return arguments;
    }


    public static List<Double> RaisingGenerator() {


        List<Double> arguments = new ArrayList<Double>();

        for (int n = -5; n < 5; n++) {
            arguments.add(Math.PI / 4 + Math.PI * 2 * n);
            arguments.add(Math.PI / 4 + Math.PI / 2 + Math.PI * 2 * n);

        }

        return arguments;
    }


    public static List<Double> DecreaseGenerator() {


        List<Double> arguments = new ArrayList<Double>();

        for (int n = -5; n < 5; n++) {
            arguments.add(Math.PI * 2 * n);
            arguments.add(-Math.PI / 2 + Math.PI / 4 + Math.PI * 2 * n);


        }

        return arguments;
    }


    @ParameterizedTest
    @MethodSource("RaisingGenerator")
    void RaisingTest(double textX) {
        SampleTanTest(textX);
    }

    @ParameterizedTest
    @MethodSource("DecreaseGenerator")
    void DecreaseTest(double textX) {
        SampleTanTest(textX);
    }

    @ParameterizedTest
    @MethodSource("RootsGenerator")
    void RootsTest(double textX) {
        SampleTanTest(textX);
    }

    @ParameterizedTest
    @MethodSource("AsyptsGenerator")
    void AsyptsTest(double textX) {
        NanTanTest(textX);
    }

    void SampleTanTest(double testX) {
        double perc = 0.01;
        Assertions.assertEquals(Math.tan(testX), tan.Calculate(testX, perc), perc);
    }

    void NanTanTest(double testX) {
        double perc = 0.01;
        Assertions.assertEquals(Double.NaN, tan.Calculate(testX, perc), perc);
    }
}