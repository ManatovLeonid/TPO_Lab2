import Abstracts.*;
import Functions.*;
import TaskTesting.TaskSystem;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Scanner;

public class main {
    private static double lower = -333.d;
    private static double upper = 333.d;

    public static double readInput(Scanner scn, String prompt) {
        while (true) {
            System.out.println(prompt);
            String line = scn.nextLine();
            try {
                double val = Double.parseDouble(line);
                if (val < lower || val > upper) {
                    System.out.println("Try again, bad value");
                    continue;
                }
                return val;
            } catch (NumberFormatException e) {
                System.out.println("Wrong number");
                continue;
            }


        }
    }

    public static void main(String[] args) {
        boolean stop1 = false;
        AbstractCos cos = new Cos();
        AbstractSin sin = new Sin(cos);
        AbstractTan tan = new Tan(cos, sin);
        AbstractLn ln = new Ln();
        AbstractLog log2 = new Log(ln, 2);
        AbstractLog log3 = new Log(ln, 3);
        AbstractLog log5 = new Log(ln, 5);
        AbstractLog log10 = new Log(ln, 10);
        AbstractSec sec = new Sec(cos);

        Scanner read = new Scanner(System.in);
        HashSet<Integer> funcs = new HashSet<>();

        TaskSystem func = new TaskSystem(cos, sin, tan, sec, ln, log2, log3, log5, log10);

        while (!stop1) {
            System.out.print("Selected options: ");
            for (Integer k :
                    funcs) {
                System.out.printf("%d ", k);
            }
            System.out.printf("\n");
            System.out.println("Enter functions:\n1.Cos x\n2.Sin x\n3.Tan x\n4.Sec x\n5.Ln x\n6.Log_2 x\n7.Log_3 x\n8.Log_5 \n9.Log_10 x\n10.F(x)\n0.Finish\n\n");
            String readen = read.nextLine();
            try {
                int readenVal = Integer.parseInt(readen);
                if (readenVal > 10 || readenVal < 0) {
                    System.out.println("Bad option\n");
                    continue;
                }
                if (readenVal == 0) break;
                if (funcs.contains(readenVal)) {
                    funcs.remove(readenVal);
                } else {
                    funcs.add(readenVal);
                }
            } catch (NumberFormatException e) {
                System.out.println("Wrong number");
                continue;
            }
        }
        System.out.print("Selected options: ");
        for (Integer k :
                funcs) {
            System.out.printf("%d ", k);
        }
        System.out.println();
        double low, up, step;
        double eps;
        low = readInput(read, "Enter low bound:");
        up = readInput(read, "Enter up bound:");
        step = readInput(read, "Enter step:");
        eps = readInput(read, "Enter epsilon:");
        StringBuilder buffer = new StringBuilder();
        for (double val = low; val < up; val += step) {
            for (Integer k :
                    funcs) {
                switch (k) {
                    case 1:
                        buffer.append(String.format("Cos x;%.8f;%.8f\n", val, cos.Calculate(val, eps)));
                        break;
                    case 2:
                        buffer.append(String.format("Sin x;%.8f;%.8f\n", val, sin.Calculate(val, eps)));
                        break;
                    case 3:
                        buffer.append(String.format("Tan x;%.8f;%.8f\n", val, tan.Calculate(val, eps)));
                        break;
                    case 4:
                        buffer.append(String.format("Sec x;%.8f;%.8f\n", val, sec.Calculate(val, eps)));
                        break;
                    case 5:
                        buffer.append(String.format("Ln x;%.8f;%.8f\n", val, ln.Calculate(val, eps)));
                        break;
                    case 6:
                        buffer.append(String.format("Log2 x;%.8f;%.8f\n", val, log2.Calculate(val, eps)));
                        break;
                    case 7:
                        buffer.append(String.format("Log3 x;%.8f;%.8f\n", val, log3.Calculate(val, eps)));
                        break;
                    case 8:
                        buffer.append(String.format("Log5 x;%.8f;%.8f\n", val, log5.Calculate(val, eps)));
                        break;
                    case 9:
                        buffer.append(String.format("Log10 x;%.8f;%.8f\n", val, log10.Calculate(val, eps)));
                        break;
                    case 10:
                        buffer.append(String.format("F x;%.8f;%.8f\n", val, func.GetValue(val)));
                        break;
                }
            }
        }
        try {
            FileWriter wr = new FileWriter("output.csv");
            wr.write(buffer.toString());
            wr.flush();
            wr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
